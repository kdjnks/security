import React from "react";
import axios from "axios";
import Cookies from "universal-cookie";

export default class Home extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            message: ''
        };
    }

    componentWillMount() {
        this.loadData();
    }

    loadData = () => {
        const cookies = new Cookies();

        axios.get('http://localhost:8080/info', { headers: {'Authorization': 'Bearer ' + cookies.get('jwtToken')}})
            .then(response => {
                this.setState({message: response.data});
            })
            .catch(error => {
                console.log(error);
                cookies.remove('jwtToken');
            });
    }

    render() {
        return (
            <div>
                <h1>Home</h1>
                <p>{this.state.message}</p>
            </div>
        )}
};