import React, { useState } from "react";
import {Button, FormGroup, FormControl, FormLabel} from "react-bootstrap";
import axios from "axios";
import Cookies from "universal-cookie"

import "./Login.css";
import { useHistory } from "react-router-dom";

const Login = () => {
    const history = useHistory();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    function validateForm() {
        return email.length > 0 && password.length > 0;
    }

    function handleSubmit(event) {
        event.preventDefault();

        axios.post('http://localhost:8080/authenticate', {username: email, password: password}, {
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .then(function (response) {
                const cookies = new Cookies();
                cookies.set('jwtToken', response.data.token, { path: '/' });
                history.push("/");
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    return (
        <div className="Login">
            <form onSubmit={handleSubmit}>
                <FormGroup controlId="email">
                    <label>Email</label>
                    <FormControl
                        autoFocus
                        type="text"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                </FormGroup>
                <FormGroup controlId="password">
                    <label>Password</label>
                    <FormControl
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        type="password"
                    />
                </FormGroup>
                <Button block disabled={!validateForm()} type="submit">
                    Login
                </Button>
                <FormLabel/>
            </form>
        </div>
    );
};

export default Login;