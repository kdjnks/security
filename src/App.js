import React from 'react';
import './App.css';
import Login from "./Login";
import {Route, Switch} from "react-router";
import Home from "./Home";

const App = () => {
  return (
      <Switch>
          <Route exact path="/">
              <Home />
          </Route>
          <Route exact path="/login">
              <Login />
          </Route>
      </Switch>
  );
}

export default App;
